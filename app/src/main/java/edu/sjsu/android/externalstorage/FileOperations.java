package edu.sjsu.android.externalstorage;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import android.util.Log;
import android.os.Environment;

public class FileOperations {
    private File path;

    public FileOperations(String path){
        this.path = new File(path);
    }

    public Boolean write(String fname, String fcontent){
        try{
            File file = new File(path, fname);

            // If file does not exists, then create it
            if(!file.exists()){
                file.createNewFile();
            }

            FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(fcontent);
            bufferedWriter.close();

            Log.d("Success", "Success");
            return true;
        }catch (IOException e){
            e.printStackTrace();
            return false;
        }
    }

    public String read(String fname){
        BufferedReader bufferedReader = null;
        String response = null;

        try{
            StringBuffer output = new StringBuffer();
            File file = new File(path, fname);

            bufferedReader = new BufferedReader(new FileReader(file));
            String line = "";
            while((line = bufferedReader.readLine()) != null){
                output.append(line + "\n");
            }
            response = output.toString();
        }catch (IOException e){
            e.printStackTrace();
            return  null;
        }
        return response;
    }
}
